Ton Agent
=========

A program to record system resource usage and to send it
to another program called Ton Office for long-term storage.

Features
--------

* Recording processor, memory, swap, network, storage usage and disk IO.
* Recording in every minute.

Configuration
-------------

`config.js` contains the configuration.

Scripts
-------

* `./restart.sh` - start/restart the program.
* `./stop.sh` - stop the program.
* `./clean.sh` - clean the program after an unexpected shutdown.
* `./rotate.sh` - clean old logs.
