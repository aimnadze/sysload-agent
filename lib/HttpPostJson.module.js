const http = require('http')
const https = require('https')

module.exports = app => (location, params, done, error, options) => {

    if (typeof error !== 'function') throw new Error
    if (options === undefined) options = {}

    const request_module = location.scheme === 'https' ? https : http

    const req = request_module.request({
        ...location,
        method: 'post',
        path: location.path,
        headers: {
            'Content-Type': 'application/json',
            ...options.headers,
        },
    }, res => {
        app.ReadText(res, responseText => {

            abort_timeout()
            req.off('error', error)
            req.on('error', () => {})

            let response
            try {
                response = JSON.parse(responseText)
            } catch {
                error({ code: 'INVALID_JSON' })
                return
            }

            done(response)

        })
    })
    req.end(JSON.stringify(params))
    req.on('error', error)

    const abort_timeout = app.Timeout(() => {
        req.abort()
    }, options.timeout || 30 * 1000)

}
