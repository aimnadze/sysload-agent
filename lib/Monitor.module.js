module.exports = app => () => {

    function next () {
        date.setUTCMinutes(date.getUTCMinutes() + 1)
        app.Timeout(() => {
            app.Parallel({
                os: app.Read.OS,
                swap: app.Read.Swap,
                network: app.Read.Network,
                storage: app.Read.Storage,
                io: app.Read.IO,
            }, result => {
                send_queue({
                    time: date.getTime() / 1000,
                    ...result,
                })
                next()
            })
        }, date.getTime() - Date.now())
    }

    const send_queue = app.SendQueue()

    const date = new Date
    date.setUTCMilliseconds(0)
    date.setUTCSeconds(0)

    next()

}
