const child_process = require('child_process')

let prev_time = 0
let prev_read = 0
let prev_write = 0

module.exports = app => done => {
    const time = Date.now()
    const iostat = child_process.spawn('iostat', ['-d'])
    app.ReadText(iostat.stdout, stdout => {

        let read = 0
        let write = 0

        const lines = stdout.split(/\n/)
        lines.shift()
        lines.shift()

        const header_line = lines.shift().split(/\s+/)
        const read_index = header_line.indexOf('kB_read')
        const write_index = header_line.indexOf('kB_wrtn')

        lines.pop()
        lines.forEach(line => {
            const values = line.replace(/^\s+/, '').split(/\s+/)
            read = Math.max(read, app.UnsignedInteger(values[read_index] * 1024))
            write = Math.max(write, app.UnsignedInteger(values[write_index] * 1024))
        })

        const result = (() => {

            if (prev_time === 0) return { read: 0, write: 0 }

            const seconds = (time - prev_time) / 1000

            return {
                read: Math.round((read - prev_read) / seconds),
                write: Math.round((write - prev_write) / seconds),
            }

        })()

        prev_time = time
        prev_read = read
        prev_write = write

        done(result)

    })
}


