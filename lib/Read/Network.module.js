const fs = require('fs')

let prev_time = 0
let prev_send = 0
let prev_receive = 0

module.exports = app => done => {
    const time = Date.now()
    fs.readFile('/proc/net/dev', 'utf8', (err, content) => {

        let send = 0
        let receive = 0

        const lines = content.split(/\n/)
        lines.shift()
        lines.shift()
        lines.pop()
        lines.forEach(line => {

            const values = line.replace(/^\s+/, '').split(/:?\s+/)
            if (values[0] === 'lo') return

            send += app.UnsignedInteger(values[9])
            receive += app.UnsignedInteger(values[1])

        })

        const result = (() => {

            if (prev_time === 0) return { send: 0, receive: 0 }

            const seconds = (time - prev_time) / 1000

            return {
                send: Math.round((send - prev_send) / seconds),
                receive: Math.round((receive - prev_receive) / seconds),
            }

        })()

        prev_time = time
        prev_send = send
        prev_receive = receive

        done(result)

    })
}
