const os = require('os')

module.exports = done => {
    process.nextTick(() => {
        done({
            loadavg: os.loadavg(),
            totalmem: os.totalmem(),
            freemem: os.freemem(),
            num_cpus: os.cpus().length,
        })
    })
}
