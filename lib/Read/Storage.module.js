const child_process = require('child_process')

module.exports = app => done => {
    const df = child_process.spawn('df', ['-T'])
    app.ReadText(df.stdout, stdout => {

        let total = 0
        let used = 0

        const lines = stdout.split(/\n/)
        lines.shift()
        lines.pop()
        lines.forEach(line => {
            const values = line.replace(/^\s+/, '').split(/\s+/)
            const type = values[1]
            if (['devtmpfs', 'tmpfs'].indexOf(type) !== -1) return
            total += app.UnsignedInteger(values[2])
            used += app.UnsignedInteger(values[3])
        })

        done({ total, used })

    })
}


