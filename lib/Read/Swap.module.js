const fs = require('fs')

module.exports = app => done => {
    fs.readFile('/proc/swaps', 'utf8', (err, content) => {

        const lines = content.split(/\n/)
        lines.shift()
        lines.pop()

        let total = 0
        let used = 0
        lines.forEach(line => {
            const values = line.replace(/^\s+/, '').split(/:?\s+/)
            total += app.UnsignedInteger(values[2])
            used += app.UnsignedInteger(values[3])
        })

        done({ total, used })

    })
}
